#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>

using namespace std;

class Imagem {

    private:

           string largura;
           string altura;
           string numero_magico;
           string valor_maximo_da_cor;


    public:
            Imagem();
            Imagem(string numero_magico, string altura, string largura, string valor_maximo_da_cor);
           ~Imagem();

           void setLargura(string largura);
           string getLargura();
           void setAltura(string altura);
           string getAltura();
           void setNumeroMagico(string numero_magico);
           string getNumeroMagico();
           void setValorMaximoDaCor(string valor_maximo_da_cor);
           string getValorMaximoDaCor();

};
#endif
