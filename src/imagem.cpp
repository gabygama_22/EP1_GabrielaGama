#include "imagem.hpp"
#include<string>

using namespace std;

Imagem::Imagem()
{

  numero_magico = "";
  altura = "";
  largura = "";
  valor_maximo_da_cor = "";

}

Imagem::Imagem(string numero_magico, string altura, string largura, string valor_maximo_da_cor)
{
        this-> numero_magico = numero_magico;
        this-> altura = altura;
        this-> largura = largura;
        this-> valor_maximo_da_cor = valor_maximo_da_cor;
}

Imagem::~Imagem()
{
}

void Imagem::setAltura (string altura)
{
  this -> altura =  altura ;
}

string Imagem::getAltura ()
{
  return altura ;
}

void Imagem::setLargura (string largura)
{
  this -> largura = largura;
}

string Imagem::getLargura ()
{
  return largura;
}

void Imagem::setNumeroMagico (string numero_magico)
{
  this -> numero_magico = numero_magico;
}

string Imagem::getNumeroMagico()
{
  return numero_magico;
}

void Imagem::setValorMaximoDaCor (string valor_maximo_da_cor)
{
  this -> valor_maximo_da_cor = valor_maximo_da_cor;
}

string Imagem::getValorMaximoDaCor ()
{
  return valor_maximo_da_cor;
}
